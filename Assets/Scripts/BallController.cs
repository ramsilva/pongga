﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {
    private float speed = 2.0f;

	// Use this for initialization
	void Start () {
        resetGame();
    }
	
	// Update is called once per frame
	void Update () {
	}

    void OnCollisionEnter2D(Collision2D col) {
        if (col.gameObject.tag == "paddle")
        {
            Vector2 velocity = GetComponent<Rigidbody2D>().velocity;
            velocity.y = calculateHitDir(col.collider) * speed;
            Debug.Log(velocity.y);
            GetComponent<Rigidbody2D>().velocity = velocity;
        }
        else if (col.gameObject.tag == "game_over") {
            resetGame();
            GameObject.FindGameObjectWithTag("paddle").GetComponent<PaddleController>().resetGame();
        }
    }

    private float calculateHitDir(Collider2D paddle) {
        return (transform.position.y - paddle.transform.position.y) / paddle.bounds.size.y;
    }

    private void resetGame() {
        transform.position = Camera.main.ViewportToWorldPoint(new Vector2(0.5f, 0.5f));
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        Vector2 startingDir = Vector2.right;
        startingDir.y = Random.Range(-100, 100) / 100.0f;
        GetComponent<Rigidbody2D>().velocity = startingDir * speed;
    }
}
