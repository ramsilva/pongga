﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddleController : MonoBehaviour {

    private int direction = 0;
    private float speed = 0.05f;
    private bool hasInit = false;

	// Use this for initialization
	void Start () {
        resetGame();
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.S)) {
            direction = 0;
        }
        if (Input.GetKeyDown(KeyCode.W)){
            direction = 1;
        }
        if (Input.GetKeyDown(KeyCode.S)) {
            direction = -1;
        }

        transform.position = new Vector2(transform.position.x, transform.position.y + speed * direction);
        if (transform.position.y + GetComponent<SpriteRenderer>().bounds.size.y / 2 > Camera.main.ViewportToWorldPoint(new Vector2(0.0f, 1.0f)).y) {
            transform.position = new Vector2(transform.position.x, Camera.main.ViewportToWorldPoint(new Vector2(0.0f, 1.0f)).y - GetComponent<SpriteRenderer>().bounds.size.y / 2);
        }
        if (transform.position.y - GetComponent<SpriteRenderer>().bounds.size.y / 2 < Camera.main.ViewportToWorldPoint(new Vector2(0.0f, 0.0f)).y){
            transform.position = new Vector2(transform.position.x, Camera.main.ViewportToWorldPoint(new Vector2(0.0f, 0.0f)).y + GetComponent<SpriteRenderer>().bounds.size.y / 2);
        }

    }

    public void resetGame() {
        transform.position = Camera.main.ViewportToWorldPoint(new Vector2(0.05f, 0.5f));
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);

        if (!hasInit) {
           transform.localScale = new Vector2(1.0f, 0.2f * CameraUtils.GetHeight() / GetComponent<SpriteRenderer>().bounds.size.y);
            hasInit = true;
        }

    }
}
