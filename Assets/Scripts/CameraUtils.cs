﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraUtils : MonoBehaviour {

    public static float GetHeight() {
        return Camera.main.orthographicSize * 2;
    }

    public static float GetWidth() {
        return Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0.0f)).x -
            Camera.main.ScreenToWorldPoint(new Vector2(0.0f, 0.0f)).x;
    }
}
