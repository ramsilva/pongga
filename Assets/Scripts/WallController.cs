﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject w1 = GameObject.Find("wall1");
        w1.transform.position = Camera.main.ViewportToWorldPoint(new Vector2(0f, 0.5f));
        w1.transform.position = new Vector3(w1.transform.position.x, w1.transform.position.y, 0);
        w1.transform.localScale = new Vector2(0.1f, CameraUtils.GetHeight() / w1.GetComponent<SpriteRenderer>().sprite.bounds.size.y);

        GameObject w2 = GameObject.Find("wall2");
        w2.transform.position = Camera.main.ViewportToWorldPoint(new Vector2(1.0f, 0.5f));
        w2.transform.position = new Vector3(w2.transform.position.x, w2.transform.position.y, 0);
        w2.transform.localScale = new Vector2(0.1f, CameraUtils.GetHeight() / w1.GetComponent<SpriteRenderer>().sprite.bounds.size.y);

        GameObject w3 = GameObject.Find("wall3");
        w3.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1.0f));
        w3.transform.position = new Vector3(w3.transform.position.x, w3.transform.position.y, 0);
        w3.transform.localScale = new Vector2(CameraUtils.GetWidth() / w1.GetComponent<SpriteRenderer>().sprite.bounds.size.x, 0.1f);

        GameObject w4 = GameObject.Find("wall4");
        w4.transform.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.0f));
        w4.transform.position = new Vector3(w4.transform.position.x, w4.transform.position.y, 0);
        w4.transform.localScale = new Vector2(CameraUtils.GetWidth() / w1.GetComponent<SpriteRenderer>().sprite.bounds.size.x, 0.1f);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
